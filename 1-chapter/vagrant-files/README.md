# Setting Up a Server for Testing
You need to have SSH access and root privileges on a Linux server.

Downloading and running your server for Testing
```
vagrant up
```

Log into your server
```
vagrant ssh
```

Ansible needs to connect with ssh longin to the virtual server by using the regular SSH client. Tell Vagrant to output its SSH configuration by typing the following command
```
vagrant ssh-config
```

trying ssh session with this following command
```
ssh vagrant@ip-optained-with-ssh-config -p 2222 -i .vagrant/machines/default/virtualbox/private_key
````

# Destroy the testing server
```
vagrant destroy -f
```