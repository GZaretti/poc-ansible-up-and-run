# Telling Ansible About Your Test Server
-m is module name
-a is arguments
-b become user (super user)
```
ansible testserver -m ping
ansible testserver -a uptime
ansible testserver -a "tail /var/log/dmesg"
ansible testserver -a "echo toto"
ansible testserver -b -a "tail /var/log/syslog"
```

## installing nginx
´´´
ansible testserver -b -m package -a name=nginx
ansible testserver -b -m service -a "name=nginx state=restarted"
´´´
